#!/bin/bash

sudo dnf install magic-wormhole java-11-openjdk-devel git -y
git clone https://gitlab.com/twodollarblake/collatz
cd collatz
javac *.java
java CollatzOrganizer
